import React from 'react';
import './App.css';

function Header(){
  return (
  <div className="header">
    <div className="headerLogo">
      <img src="./favicon.png"></img>  
    </div>
    <div className="headerText">
      <h4>kit-bucket.ru</h4>
    </div>
  </div>
  )
}

class App extends React.Component {
  renderHeader() {
    return (
      <Header />
    )
  }
  render () {
    return (
      this.renderHeader()
    )
  }
}


export default App;
